﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding
{
    
    // pathfinding
    // - qué es? algoritmos de búsqueda en grafos utilizados para obtener una ruta entre 2 nodos

    // busqueda a lo ancho
    // breadthwise search
    public static List<Nodo> Ancho(Nodo inicio, Nodo fin){

        Queue<Nodo> trabajo = new Queue<Nodo>();
        List<Nodo> visitados = new List<Nodo>();

        inicio.historial = new List<Nodo>();

        trabajo.Enqueue(inicio);
        visitados.Add(inicio);

        while(trabajo.Count > 0){

            Nodo actual = trabajo.Dequeue();

            if(actual == fin){

                // si sí fue se regresa el historial del nodo junto con el mismo
                List<Nodo> resultado = new List<Nodo>(actual.historial);
                resultado.Add(actual);
                return resultado;
            } else {

                // de no ser así explorar los hijos / vecinos
                for(int i = 0; i < actual.vecinos.Length; i++){

                    Nodo vecinoActual = actual.vecinos[i];

                    // verificar si no fue visitado
                    if(!visitados.Contains(vecinoActual)){
                        // si es "nuevo" entonces procesarlo

                        // reiniciar historial del vecino
                        vecinoActual.historial = new List<Nodo>(actual.historial);
                        vecinoActual.historial.Add(actual);

                        // agregar a las 2 estructuras
                        visitados.Add(vecinoActual);
                        trabajo.Enqueue(vecinoActual);

                    }
                }
            }

        }

        return null;
    }

    // busqueda a lo profundo
    // depthwise
    public static List<Nodo> Profundo(Nodo inicio, Nodo fin){
        return null;
    }

    // A Star
    // A estrella
    public static List<Nodo> AEstrella(Nodo inicio, Nodo fin){
        return null;
    }
}
