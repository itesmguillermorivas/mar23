﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{

    public Nodo[] ruta;
    public float velocidad = 5;
    public float rangoValido;

    private int nodoActual;

    // Start is called before the first frame update
    void Start()
    {
        nodoActual = 0;
        StartCoroutine(VerificarDestino());
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(ruta[nodoActual].transform);
        transform.Translate(transform.forward * Time.deltaTime * velocidad, Space.World);
    }


    IEnumerator VerificarDestino(){

        // intención: verificar qué tan cerca está de su destino actual para ver si cambiamos rumbo
        while(true){

            float distancia = Vector3.Distance(transform.position, ruta[nodoActual].transform.position);

            if(distancia < rangoValido){

                nodoActual++;
                nodoActual %= ruta.Length;
            }

            yield return new WaitForSeconds(0.3f);
        }
    }
}
