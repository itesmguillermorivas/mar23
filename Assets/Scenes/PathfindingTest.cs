﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingTest : MonoBehaviour
{
    public Nodo inicio, fin;

    // Start is called before the first frame update
    void Start()
    {
        List<Nodo> ruta = Pathfinding.Ancho(inicio, fin);

        for(int i = 0; i < ruta.Count; i++){

            Debug.Log(ruta[i].transform.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
